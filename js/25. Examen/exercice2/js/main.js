'use strict';
/**
 * @var {Array} tabNum1 tableau avec des chiffres à multiplier.
 */
const tabNum1 = [4,8,15,16,23,42];

/**
 * @var {Array} tabNum2 tableau avec des chiffres à multiplier.
 */
const tabNum2 = [65,32,456,12,64,23,94,12];

/**
 * @var {Number} result résultat de la multiplication
 */
let result;

/**
 * @var {Element} body on cible l'endroit où on veut écrire.
 */
let resultDisplay;

/**
 * @var {Element} textDiv Balise p qui récupère le message.
 */
let textDiv;

/**
 * @var {Element} multiplication Ecriture de la multiplication.
 */
let multiplication;


// On charge le DOM
window.addEventListener('DOMContentLoaded',()=>{
    tabNum1.forEach((num1) => {
        tabNum2.forEach((num2)=>{
            result = num1 * num2
            resultDisplay =  document.querySelector('body section');
            textDiv = document.createElement('p');
            multiplication = document.createTextNode(`${num1} * ${num2} = ${result}`);
    
        // on met le texte/message dans la balise p
        textDiv.appendChild(multiplication);
        // on insère la balise p dans le body
        resultDisplay.appendChild(textDiv);
        })
    })
})