'use strict';


let nodeListMenu;

let nodeListSection;


document.addEventListener('DOMContentLoaded',()=>{
    nodeListMenu = document.querySelectorAll('nav a');
    nodeListSection = document.querySelectorAll('section');
    nodeListMenu.forEach((menu)=>{
        menu.addEventListener('click',switchPage);
    })
})


/**
 * fonction permettant de changer le contenu ainsi que la mise en avant du menu
 */
function switchPage() {
    removeSelected();
    this.classList.add("current");
    nodeListSection.forEach((idSection)=>{
        if(idSection.id == this.dataset.id){
            idSection.classList.add("current");
        };
    }
)}


/**
 * fonction faisant un reset de tous les objets sélectionnés.
 */
function removeSelected(){
    nodeListMenu.forEach((menu) => {
        menu.classList.remove('current');
    });
    nodeListSection.forEach((section) => {
        section.classList.remove('current');
    });

}