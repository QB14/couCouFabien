'use strict';


function writeHello(){
    /**
     * @var {Element} body on cible l'endroit où on veut écrire.
     */
    let body =  document.querySelector('body');

        /**
     * @var {Element} textDiv Balise p qui récupère le message.
     */
    let textDiv = document.createElement('p');

        /**
     * @var {Element} text message à écrire.
     */
    let text = document.createTextNode('bonjour');

    // on met le texte/message dans la balise p
    textDiv.appendChild(text);
    // on insère la balise p dans le body
    body.appendChild(textDiv);
}