'use strict';


/**
 * 
 * @param {String} targetName sélectionne l'élément du DOM sur lequel ajouter l'évènement
 * @param {String} eventType ajoute un évènement
 * @param {Function} listener déclenche la fonction associée à l'event.
 */
function addEventToElement(targetName, eventType, listener){
document.querySelector(targetName).addEventListener(eventType,listener);
}

function ok(){
    console.log('ok');
}


// On charge le DOM
window.addEventListener('DOMContentLoaded',()=>{

    addEventToElement('button','click',ok);

})