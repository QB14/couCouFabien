'use strict';


const image = ['chaton.jpg','chaton2.jpg','meme.jpg','mind.gif'];

/**
 * @var {Element} imagesCollection Objet du DOM Nodelist affecté après le chargement du DOM
 */
let imagesCollection;


/**
 * @var {Element} overlay Objet du DOM affecté après le chargement du DOM
 */
let overlay;

let buttonClose;




function zoomImage(){
    overlay.classList.toggle('toggleOverlay');
    if(overlay.classList.contains('toggleOverlay')== false){
        overlay.querySelector('img').src = `assets/images/${image[this.dataset.id]}`;
    }
}




document.addEventListener('DOMContentLoaded',()=>{
    imagesCollection = document.querySelectorAll('body>img');
    overlay= document.querySelector('#overlay');
    buttonClose = document.querySelector('#overlay button');
    imagesCollection.forEach((image)=>{
        image.addEventListener('click',zoomImage);
    })
    overlay.addEventListener('click',zoomImage);
    buttonClose.addEventListener('click',zoomImage);
})

